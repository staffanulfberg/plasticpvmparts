include <BOSL/constants.scad>
use <BOSL/transforms.scad>
use <BOSL/shapes.scad>

$fn=32;

back_plate_height = 26.5;
back_plate_width = 73;
back_plate_thickness = 2.9;
back_plate_fillet = 1;
back_plate_hole_dist = 77;
back_plate_hole_radius = 1.75;
back_plate_ear_radius = 3.8;
back_cutout_v_border=5;
back_cutout_h_border=5.8;
back_cutout_depth=4.7;
back_cutout_v_narrowing=2.5;
back_cutout_h_narrowing=2.5;
insert_width = 63.2;
insert_height = 17.4;
insert_back_depth = 4;

// orthogonal to y axis, with the inner part of the back insert touching the y=0 plane
// centered
module back_base() {
    difference() {
        ymove(-insert_back_depth) {
            cuboid(size = [back_plate_width, back_plate_thickness, back_plate_height],
                fillet=back_plate_fillet, edges=EDGES_Y_ALL, align=V_FRONT);

            ymove(-0.01)
            cuboid(size = [insert_width, insert_back_depth, insert_height], align=V_BACK);
            
            for (i=[-1:2:1])
                xmove(i * back_plate_hole_dist / 2)
                    difference() {
                        union() {
                            ycyl(l=back_plate_thickness, r=back_plate_ear_radius, align=V_FRONT);
                            xmove(i * -back_plate_ear_radius/2)
                            cuboid(size=[back_plate_ear_radius, back_plate_thickness,
                                         2*back_plate_ear_radius], align=V_FRONT);
                        }
                        ymove(0.01)
                            ycyl(l=back_plate_thickness+0.02, r=back_plate_hole_radius, align=V_FRONT);
        
                    }
        }
        
        ymove(-insert_back_depth-back_plate_thickness-0.01)
            rounded_prismoid(size1=[back_plate_width-back_cutout_h_border*2, back_plate_height-back_cutout_v_border*2],
                size2=[back_plate_width-back_cutout_h_border*2-back_cutout_h_narrowing*2, 
                       back_plate_height-back_cutout_v_border*2-back_cutout_v_narrowing*2],
                h=back_cutout_depth, r=back_plate_fillet, orient=ORIENT_Y, align=V_BACK);
    }
}


bottom_thickness=2.0;
side_wall_thickness=1.6;
back_wall_thickness=1.6;
back_wall_left_indent=2.0;
compartment_inner_depth=56;
inner_wall_thickness=1.2;
terminal_thickness=0.85;
terminal_fastener_thickness=1.2;
terminal_fastener_width=3;
terminal_rest_height=1.35;
back_wall_pole_cutout_radius=5.1;

inner_wall_spacing=(insert_width-2*side_wall_thickness-3*inner_wall_thickness)/4+inner_wall_thickness;
terminal_width=26.0;
terminal_spacer_width=(2*inner_wall_spacing-inner_wall_thickness-terminal_width)/2;

module main_compartment() {
    // bottom plate
    zmove(-insert_height/2)
        cuboid(size = [insert_width, compartment_inner_depth, bottom_thickness], align=[0, 1, 1]);

    for (i=[-1:2:1]) // sides
        xmove(i * (insert_width/2 - side_wall_thickness/2))
        cuboid(size = [side_wall_thickness, compartment_inner_depth+0.02, insert_height], align=V_BACK);

    // Notice the back wall is "doubled" on the left side with the indent.  This is cut away later.
    ymove(compartment_inner_depth) // back wall
    cuboid(size = [insert_width, back_wall_thickness, insert_height], align=V_BACK);

    ymove(compartment_inner_depth-back_wall_left_indent) // back left indent part
    xmove((inner_wall_spacing+side_wall_thickness)/2-insert_width/2)
    cuboid(size = [inner_wall_spacing+side_wall_thickness, back_wall_thickness, insert_height], align=V_BACK);
    
    // inner walls, with fasteners for the battery terminals
    for (i=[-1:1]) { // three inner walls/terminals (i*i is 0 for the middle and 1 for the sides)
        // inner walls
        xmove(i * inner_wall_spacing)
        ymove(i*i*terminal_thickness) // space for the battery terminal
        cuboid(size = [inner_wall_thickness, compartment_inner_depth-terminal_thickness+0.02,
                       insert_height], align=V_BACK);
                      
        // fasteners on each side of the terminals
        xmove(i * inner_wall_spacing)
        ymove((1-i*i)*(compartment_inner_depth-terminal_fastener_thickness-2*terminal_thickness)+terminal_thickness)
        for (j=[-1:2:1])
            xmove(j*(inner_wall_spacing-inner_wall_thickness/2-terminal_fastener_width/2))
            cuboid(size = [terminal_fastener_width+0.02, terminal_fastener_thickness+0.02, insert_height], 
                   align=V_BACK);
        
        // stoppers to keep the terminals sidaways
        xmove(i * inner_wall_spacing)
        ymove((1-i*i)*(compartment_inner_depth-terminal_thickness))
        for (j=[-1:2:1])
            xmove(j*(inner_wall_spacing-inner_wall_thickness/2-terminal_spacer_width/2))
            cuboid(size = [terminal_spacer_width+0.02, terminal_fastener_thickness, insert_height], 
                   align=V_BACK);
                   
        // stoppers so the terminals do not fall all the way down
        zmove(-insert_height/2+bottom_thickness+terminal_rest_height/2)
        xmove(i * inner_wall_spacing)
        ymove((1-i*i)*(compartment_inner_depth-terminal_thickness))
        cuboid(size = [inner_wall_spacing*2, terminal_thickness, terminal_rest_height], align=V_BACK);
    }
    
    // text / battery decoration
    zmove(-insert_height/2+bottom_thickness) ymove(compartment_inner_depth/2)
    for (i=[0:3])
        xmove(inner_wall_spacing * i - inner_wall_spacing * 1.5)
        linear_extrude(height=0.5) 
            zrot(90 + i % 2 * 180) {
                text("+ AA -", size=5, valign="center", halign="center");
            }

}

module compartment_cutouts() {
    // battrey pole holes
    for (i=[-1:2:1])
        ymove(compartment_inner_depth-5.0)
        xmove(i * inner_wall_spacing*1.5)
        zmove(bottom_thickness/2)
        ycyl(l=10.0, r=back_wall_pole_cutout_radius, align=V_BACK);

    // left indent
    ymove(compartment_inner_depth+back_wall_thickness-back_wall_left_indent)
    xmove((inner_wall_spacing+side_wall_thickness)/2-insert_width/2-0.01)
    cuboid(size = [inner_wall_spacing+side_wall_thickness, 10.0, insert_height+10], align=V_BACK);
}

ymove(0.04) back_base();
difference() {
    main_compartment();
    compartment_cutouts();
}
