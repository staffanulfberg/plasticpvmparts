## Replica of Battery Insert PWS-A01 ##

This is a replica, or at least, a working substitute, for the battery
insert PWS-A01 for the
Kern PWS 8000-1, PWS 800-2, and PWS 3000-1 precision balances.
Actually, I've never seen the original part, but printed it since
it is clearly overpriced (over 100 EUR).  It looks more or less
like the product pictures, and does fit in my PWS 8000-1.

The model is drawn in [OpenSCAD](https://openscad.org).

![OpenSCAD Screenshot](battery-insert.png)

I printed the battery insert in PETG using supports in the same material,
but I guess any material would work; infill is not important.

In addition to the plastic part, you will need 3x battery terminals -- I
ordered a bag of these from Amazon for under 10 EUR: ![Battery terminals](battery-plate.png)
If the terminals are slightly different then some small modifications to the
code will be necessary.  It is a very tight fit for the terminals in the picture
and I heated the plastic slightly to put them in place.

The seal and the screws can be re-purposed from the cover plate that covers
the battery compartment.  Optionally get some M3 thumb screws to ease battery 
replacement.


## Copyright ##

Copyright 2024 Staffan Ulfberg <staffan@ulfberg.se>

![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png) 

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
