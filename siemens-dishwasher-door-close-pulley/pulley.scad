
$fn=64;
wheel1_thickness=20;
thickness=10;
plate_thickness=4;
string_radius=2.5;
wheel_r = 18;
wheel2_r = 15;
wheel2_x = 17;
wheel2_y = 50;

union() {
    difference() {
        union() { // the two pulleys
            cylinder(h=wheel1_thickness, r=wheel_r);
            
            translate([-15, -2*wheel_r/4+20, wheel1_thickness/2]) cube([35, 2*wheel_r/3+40, wheel1_thickness], center=true); // reinfocement block
            
            translate([wheel2_x, wheel2_y, 0]) cylinder(h=thickness, r=wheel2_r);
            
            translate([19.8, 37, 10]) cube([10, 38, 20], center=true); // reinforcement block

        }
        union() { // cutout chords from the circles
            rotate(0, [0, 0, 1]) translate([-30-15, -3, -wheel1_thickness/2]) 
                cube([30, 50, wheel1_thickness*2]);
                
            translate([wheel2_x, wheel2_y, 0]) rotate(-180, [0, 0, 1]) 
                translate([-wheel2_r * 1.51, 0, thickness/2]) 
                cube([2 * wheel2_r, 2 * wheel2_r, thickness*2], center=true);

            translate([wheel2_x, wheel2_y, 0]) rotate(-180, [0, 0, 1]) 
                translate([-wheel2_r * 1.40, -23, thickness/2]) 
                cube([2 * wheel2_r, 2 * wheel2_r, thickness*2], center=true);
                
        }

        // cutout for string from first pulley (but not the hole)
        translate([0, 0, plate_thickness + string_radius]) 
            rotate(-122, [0, 0, 1]) rotate_extrude(convexity = 10, angle=203)
                translate([wheel_r + string_radius*0.3, 0, 0]) circle(r = string_radius);
    
        translate([wheel2_x, wheel2_y, 0]) // cutout for string from second pulley
            translate([0, 0, plate_thickness + string_radius]) rotate_extrude(convexity = 10, angle=260) 
                translate([wheel2_r + string_radius*0.3, 0, 0]) circle(r = string_radius);
    }

    // connecting plane
    linear_extrude(height=plate_thickness) polygon(points=[[-15, 8], [-15, 55], [24.6, 56], [24.6, 13], [0, -5]]);
//    rotate(-10, [0, 0, 1]) translate([-wheel_r*0.2, -wheel_r*0.1, 0])
//        cube([wheel2_r*1.2, wheel2_y+wheel_r*0.2+wheel2_r*0.2, plate_thickness]);
}
