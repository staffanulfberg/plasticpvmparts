## Siemens dishwasher door mechanism pulley ##

This is a part to replace the plastic pulley that the door spring
string pivots around on a Siemens dishwasher from 2016.

The spring and string asserts force that helps closing the dishwasher door,
and the force is quite strong.  Unfortunately, due to poor engineering,
the pulley, including the plastic part it is affixed to, breaks after a 
few years: this seems to be a common problem.  In my case both sides were
broken.

The model is drawn in [OpenSCAD](https://openscad.org).  Admittedly, it is not pretty; I just wanted this
to work.

![OpenSCAD Screenshot](pulley-screenshot.png)

I printed the parts in Polycarbonate (PC) without supports.  I tried using
nylon first but the pulleys were deformed after a few months.  The PC parts have lasted
three months as of writing this, and still look fine on inspection.  Use 100 % infill -- these
parts take a lot of force!

The STL file is for the left side; mirror it to print the part for the right side!

This is what the part looks like when installed: ![picture 1](installed.jpeg) ![picture 2](installed2.jpeg).
Hopefully the pictures can give a hint as to whether it will fit in your dishwasher.  There
are a lot of models so I won't make a compatibility list.  My dishwasher is an sx678x26te.

Notice that the part is not attached to the little plastic peg that holds the original pulley; it
is held in place by the string and the metal cover.  If the peg is still there you will have to
remove it to install this part.

## Copyright ##

Copyright 2024 Staffan Ulfberg <staffan@ulfberg.se>

![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png) 

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
