# Various plastic parts

Here are some 3D print designs that I made for various purposes that
might be of use to others as well.  Don't be fooled by the repository
name: it started by me making a replacement D Bracket for a Sony PVM-14L5
monitor.  It now contains more stuff.

* [D Bracket for Sony PVM-14L5](pvm14l5_dbracket/README.md)
* [Replica of Battery Insert PWS-A01 for Kern PWS 8000-1, PWS 800-2, and PWS 3000-1](kern-scale-battery-holder/README.md)
* [Siemens dishwasher door mechanism pulley](siemens-dishwasher-door-close-pulley/README.md)


## Copyright ##

Copyright 2021, 2024 Staffan Ulfberg <staffan@ulfberg.se>

![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png) 

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
