## D Bracket for Sony PVM-14L5 ##

This is an attempt att drawing a replica D bracket for the Sony
PVM-14L5 monitor in [JSCAD](https://github.com/jscad).

The D bracket sits at the left side of the Sony PVM-14L5 and holds the
D and D1 boards in place.  Unfortunately it is very common for this
part to crack or shatter completely, and new parts are not available
from Sony anymore.

![JSCAD Screenshot](jscad.png)

If you don't want to tinker with the design and only want a
replacement D Bracket, just download the stl or 3mf files.  The stl
files are exported directly from JSCAD, and for some reason at least
my slicer thinks they are broken. I used the tool at
https://tools3d.azurewebsites.net/ to fix them.  The output from the
tool is the 3mf files; those are the ones I printed.

All generated files are from the latest version of the design, so
there is no reason to regenerate them unless you want to tinker with
the JSCAD source.

Since the bracket is quite large you might not be lucky enough to have
a printer that can print it all in one piece; at least I wasn't.  For
this reason there are a few STL files: one file for the whole thing,
and also two files where the design has been cut in half so it can be
printed in two parts to be glued together.  If you want to cut the
design in two some other way then just change the code or use some
other tool to do that.

I printed the bracket in ABS using dissolvable support material; I've
not experimented with printing this without support.  The reason that
the original part is failing is probably due to the high temperature
inside the monitor, so I would suggest not using PLA.  I also used
100% infill.  Total weight is 135 g for the two pieces that are glued to gether, a bit less if printed as one part (there is some reinforcement where the pieceas are glued together).

There is an option (the variable enableCrossBars in the top of the
code controls this) to add some cross bars to the design for increased
stability. I do not really think this should be necessary but the
option is there if someone wants it.


## Copyright ##

Copyright 2021 Staffan Ulfberg <staffan@ulfberg.se>

![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png) 

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
