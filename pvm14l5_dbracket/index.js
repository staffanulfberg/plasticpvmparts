/*
 * Copyright 2021 Staffan Ulfberg <staffan@ulfberg.se>
 *
 * This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 
 * International License.
 */

const jscad = require('@jscad/modeling')
const { path2, geom2 } = jscad.geometries
const { cylinder, polygon, cuboid, roundedCuboid, roundedRectangle } = jscad.primitives
const { translate, rotateX, rotateY, rotateZ, mirrorY } = jscad.transforms
const { offset } = jscad.expansions
const { mat4 } = jscad.maths
const { subtract, union, intersect } = jscad.booleans
const { extrudeLinear, extrudeFromSlices, slice} = jscad.extrusions

// What to print
const outputSelection = 0; // 0: everything, 1: first half, 2: second half
const enableCrossBars = 0;

// Misc dimensions
const thickness = 2;
const depth = 10;
const frameWidth = 8.6;
const crossWidth = 6;
const frameSizeX = 163;
const frameSizeY = 144.4;
const shellSizeY = 142;
const indentDepth = 5;
const indentWidth = 2;
const screwHoleRadius = 5; // for d board
const screwHoleInnerRadius = 1.5;
const fastenerSizeX = 22; // for d2 board
const fastenerSizeY = 14;

// D2 board
const d2Xdist = 60;
const d2Ydist = 115;
const d2centerX = frameSizeX / 2 + d2Xdist / 2;
const d2centerY = frameSizeY / 2;
// D board screw holes
const dScrewXDist1 = 145;
const dScrewYDist1 = 130;
const dScrewXDist2 = 136.2; // ...2 is the screw hole in the beveled part
const dScrewYDist2 = 123;
const dScrewCornerX = 154.5;
const dScrewCornerY = 7;

// screw hole in the frame for the D board
function makeScrewHole(pos, orig) {
    return subtract(union(orig,
			  cylinder({center: [...pos, depth / 2], height: depth,
				    radius: screwHoleRadius - indentWidth}),
			  cylinder({center: [...pos, (depth - indentDepth) / 2], height: depth - indentDepth,
				    radius: screwHoleRadius})
			 ),
		    cylinder({center: [...pos, depth / 2], height: depth,
			      radius: screwHoleInnerRadius}));
}

function makeFastener(pos, orig) {
    const rect = roundedRectangle({size: [fastenerSizeX, fastenerSizeY], roundRadius: 1.5});
    const innerRect = roundedRectangle({size: [fastenerSizeX - 2 * thickness,
					       fastenerSizeY - 2 * thickness], roundRadius: 1.1});
    const matter = union(extrudeLinear({height: thickness}, rect),
			 extrudeLinear({height: depth - indentDepth},
				       subtract(rect, innerRect)));
    const cut = union(cuboid({center: [ 0, 0, 0], size: [6, 6, 10]}),
		      roundedCuboid({size: [1.5, 7.5, 10]}),
		      cuboid({center: [ 7.5, 0, 0], size: [4, 8, 1]}),
		      cuboid({center: [ -7.5, 0, 0], size: [4, 8, 1]}));
    return subtract(union(orig, translate(pos, matter)), translate(pos, cut));
}

// screw hole and tab for the chassis
function makeTabAndScrewHole(pos, orig) { // pos is screw hole pos at surface
    const d = 21;
    const wedge = rotateY(Math.PI / 2,
			  translate([0, 0, -thickness / 2],
				    extrudeLinear({height: thickness},
						  polygon({points: [[0, -8], [d, -3], [d, 3], [0, 8]]}))));
    const matter = union(wedge,
			 rotateZ(Math.PI / 2, wedge),
			 translate([0, 0, -d / 2], cylinder({height: d, radius: 3})),
			 translate([0, 0, 2],
				   extrudeLinear({height: thickness},
						 polygon({ points: [[-7, 28], [-7, 17], [-6, 12],
								    [6, 12], [7, 17], [7, 28]] }))),
			 cuboid({center: [0, 22.3 + 6 /2, 4 / 2], size: [14, 6, 4]}));
 
    const cut = union(cylinder({height: d * 3, radius: screwHoleInnerRadius}),
		      roundedCuboid({center: [0, 16.3, -thickness / 2],
				     size: [21, 12, thickness * 2], roundRadius: 1})
		     );

    return subtract(union(orig, translate(pos, matter)), translate(pos, cut));
}

function cutBox() {
    const zigzag = [...Array(200).keys()].map(i => [0.8 * Math.sin(i * 0.8), i - 15]);
    const s = slice.fromPoints([...zigzag, [-200, 185], [-200, -15]]);

    const cb = (progress, i, base) => {
	const t = mat4.fromTranslation(mat4.create(),
				       [progress * 3.0,
					2 * Math.sin(2 * Math.PI * progress * 4), 200 * progress - 15]);
	return slice.transform(t, base);
    }
    
    const box = extrudeFromSlices({numberOfSlices: 150, callback: cb}, s);

    return box;
}

const main = () => {
    //return subtract(cuboid({size: [15, 15, 90]}), cuboid({size: [11, 11, 90]}));
    
    const poly = polygon({ points: [[0, 0], [frameSizeX, 0], [frameSizeX, frameSizeY],
				    [24, frameSizeY], [0, 124]] });
    const indentpoly = offset({ delta: -(frameWidth - indentWidth) }, poly);
    const innerpoly = offset({ delta: -frameWidth }, poly);

    const barCenter = [frameSizeX / 2, frameSizeY / 2];

    const horizBar = cuboid({center: [...barCenter, (depth - indentDepth) / 2],
			     size: [frameSizeX, thickness, depth - indentDepth]});
    const horiz = [ translate([0, crossWidth / 2 - thickness / 2, 0], horizBar),
		    translate([0, -(crossWidth / 2 - thickness / 2), 0], horizBar),
		    cuboid({center: [...barCenter, depth - indentDepth - thickness / 2],
			    size: [frameSizeX, crossWidth, thickness]})
		  ].reduce((a, b) => union(a, b));

    const vertBarLen = d2Ydist - fastenerSizeY;
    const vertBar = cuboid({center: [...barCenter, (depth - indentDepth) / 2],
			    size: [thickness, vertBarLen, depth - indentDepth]});
    const vert = [ translate([crossWidth / 2 - thickness / 2, 0, 0], vertBar),
		   translate([-(crossWidth / 2 - thickness / 2), 0, 0], vertBar),
		   cuboid({center: [...barCenter, depth - indentDepth - thickness / 2],
			   size: [crossWidth, vertBarLen, thickness]})
		 ].reduce((a, b) => union(a, b));
    
    let frame = [
	extrudeLinear({ height: depth},
		      subtract(poly, offset({delta: -thickness}, poly))),
	extrudeLinear({ height: depth - indentDepth},
		      subtract(offset({delta: thickness}, innerpoly), innerpoly)),
	translate([0, 0, depth - indentDepth - thickness],
		  extrudeLinear({ height: indentDepth + thickness},
				subtract(offset({delta: thickness}, indentpoly), indentpoly))),
	translate([0, 0, depth - indentDepth - thickness],
		  extrudeLinear({ height: thickness},
				subtract(indentpoly, innerpoly))),
	translate([0, 0, depth - thickness],
		  extrudeLinear({height: thickness},
				subtract(poly, indentpoly))),
	horiz,
	vert,
	//translate([frameSizeX / 2 + crossWidth / 2 + fastenerSizeX / 2, frameSizeY / 2 + fastenerSizeY / 2 + crossWidth / 2 + 12, 0], fastener()),
    ].reduce((a, b) => union(a, b));

    frame = makeFastener([d2centerX - d2Xdist / 2, d2centerY - d2Ydist / 2], frame);
    frame = makeFastener([d2centerX - d2Xdist / 2, d2centerY + d2Ydist / 2], frame);
    frame = makeFastener([d2centerX + d2Xdist / 2, d2centerY - d2Ydist / 2], frame);
    frame = makeFastener([d2centerX + d2Xdist / 2, d2centerY + d2Ydist / 2], frame);

    frame = makeScrewHole([dScrewCornerX, dScrewCornerY], frame);
    frame = makeScrewHole([dScrewCornerX - dScrewXDist1, dScrewCornerY], frame);
    frame = makeScrewHole([dScrewCornerX - dScrewXDist2, dScrewCornerY + dScrewYDist2], frame);
    frame = makeScrewHole([dScrewCornerX, dScrewCornerY + dScrewYDist1], frame);

    // D board card edge stops
    frame = union(frame,
		  cuboid({center: [2, frameSizeY / 3, depth + 1], size: [4, 4, thickness]}),
		  cuboid({center: [2, 2 * frameSizeY / 3, depth + 1], size: [4, 4, thickness]}));

    const clipWidth = 8;
    frame = union(frame, // "clip" for D board -- modified so it can be printed without supports
     		  translate([frameSizeX / 2 - clipWidth / 2, 0, depth],
			    rotateX(Math.PI / 2,
				    rotateY(Math.PI / 2,
					    extrudeLinear({height: clipWidth},
							  geom2.fromPoints([[0, 0], [2, 0], [2, 2.5],
									    [6, 6], [6, 8], [0, 8]]))))));
    
    frame = makeFastener([39, frameSizeY - frameWidth - fastenerSizeY / 2 + thickness, 0], frame); // purpose?

    // Part sticking out and attaching to chassis
    let p1 = path2.fromPoints({}, [[0, 0], [8, 0], [17, 60], [22, 60]]);
    p1 = path2.appendArc({endpoint: [37, 75], radius: [15, 15], clockwise: true, segments: 64}, p1);
    p1 = path2.appendPoints([[105, 75], [105, 60], [115, 60], [144, 72], [144, 85],
			     [0, 85]], p1);
    p1 = path2.close(p1);

    const sh = rotateX(Math.PI / 2,
		       extrudeLinear({height: -thickness}, geom2.fromPoints(path2.toPoints(p1))));
    const shThicknesses = [8, 9, 8];
    const shOffsets = [0, shellSizeY / 2 - shThicknesses[1] / 2, shellSizeY - shThicknesses[2]];

    const p2 = path2.fromPoints({}, [[0, 0], [0, 85], [144, 85], [144, 85 - thickness],
				     [144 - thickness, 85 - thickness], [thickness, 85 - thickness],
				     [thickness, 0]]);

    const trusses = shThicknesses.map((t, i) =>
	union(
	    translate([0, shOffsets[i], 0],
		      rotateX(Math.PI / 2,
			      extrudeLinear({height: -t},
					    geom2.fromPoints(path2.toPoints(path2.reverse(p2)))))),
	    translate([0, shOffsets[i], 0], sh),
	    translate([0, shOffsets[i] + t - thickness, 0], sh)));

    let sideSupportPath = path2.fromPoints({}, [[-50, 10], [-36, 10]]);
    sideSupportPath = path2.appendArc({endpoint: [0, 40], radius: [37, 37], segments: 64}, sideSupportPath);
    sideSupportPath = path2.appendPoints([[0, 60], [17, 60], [8, 0], [-50, 0]], sideSupportPath);
    sideSupportPath = path2.close(sideSupportPath);
    const sideSupport = rotateX(Math.PI / 2,
				extrudeLinear({height: (frameSizeY - shellSizeY) / 2},
					      geom2.fromPoints(path2.toPoints(path2.reverse(sideSupportPath)))));

    const sideSupports = union(
	translate([0, 0, 0], sideSupport),
	translate([0, frameSizeY - (frameSizeY - shellSizeY) / 2, 0], sideSupport)
    );
    
    
    let shell = union(trusses,
		      sideSupports,
		      cuboid({center: [(105 + 144) / 2, shellSizeY / 2, 85 - thickness / 2],
				     size: [144 - 105, shellSizeY, thickness]}),
		      cuboid({center: [105 + thickness / 2, shellSizeY / 2, 85 - 25 / 2],
				     size: [thickness, shellSizeY, 25]}),
		      cuboid({center: [144 - thickness / 2, shellSizeY / 2, 85 - 13 / 2],
				     size: [thickness, shellSizeY, 13]}),
		     );

    shell = makeFastener([47, shellSizeY / 2 - frameWidth / 2 - fastenerSizeY / 2 + thickness, 75], shell); // purpose?

    shell = makeTabAndScrewHole([144-23.5, 48, 85], shell);
    shell = makeTabAndScrewHole([144-23.5, 101.5, 85], shell);

    const distances = shThicknesses.map((t, i) =>
	union(
	    roundedCuboid({center: [82 + 5 / 2, shOffsets[i] + t / 2, 85], size: [5, t, 8], roundRadius: 2}),
	    cuboid({center: [4 / 2, shOffsets[i] + t / 2, 85], size: [4, t - 4.5, 8]})));
	    
    shell = union(shell, distances);

    if (enableCrossBars != 0) {
	const crossBarThickness = 5;
	const crossBars = [0, 1].map(i => {
	    const d = shOffsets[i + 1] - shOffsets[i] - shThicknesses[i];
	    const bar = rotateZ(Math.PI / 4,
				cuboid({size: [crossBarThickness, (d + 10) * Math.sqrt(2), crossBarThickness]}));
	    const barsTooLong = union(bar, mirrorY(bar));
	    const cut = cuboid({size: [d * 2, shThicknesses[i], 30], center: [0, -d / 2 - shThicknesses[i] / 2, 0]});
	    const cuts = union(cut, mirrorY(cut));
	    const bars = subtract(barsTooLong, cuts);
	    const xOff = shOffsets[i] + shThicknesses[i] + d / 2;
	    return [ translate([crossBarThickness / 2, xOff, 85 / 2 + 6], rotateY(Math.PI / 2, bars)),
		     translate([50, xOff, 85 - crossBarThickness / 2], bars)];
	});
	shell = union(shell, crossBars);
    }
    
    if (outputSelection != 0) {
	// Reinforce for cutting in two pieces
	const reinforcement = shThicknesses.map((t, i) =>
	    translate([0, shOffsets[i], 0],
		      rotateX(Math.PI / 2,
			      extrudeLinear({height: -t},
					    polygon({points: [[0, 0], [8, 0], [17, 60], [17, 85], [0, 85]]})))));
	shell = union(shell, reinforcement);
    }
    
    const all = union(frame,
		      translate([frameSizeX - thickness, (frameSizeY - shellSizeY)/ 2, 0], shell));

    if (outputSelection == 0)
	return all;
    else {
	const cut = translate([frameSizeX + 2, 0, 0], cutBox());
	if (outputSelection == 1)
	    return subtract(all, cut);
	else
	    return intersect(all, cut);
    }
}

module.exports = { main }
